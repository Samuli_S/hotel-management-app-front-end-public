import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './core/auth/auth.guard';

import { HomeComponent } from './home/home.component';
import { EntryComponent } from './entry/entry.component';
import { SignInComponent } from './entry/sign-in/sign-in.component';
import { SignUpComponent } from './entry/sign-up/sign-up.component';
import { ReservationComponent } from './reservation/reservation.component';
import { ReservationCreatorComponent } from './reservation/reservation-creator/reservation-creator.component';
import { ReservationsViewerComponent } from './reservation/reservations-viewer/reservations-viewer.component';
import { HotelComponent } from './hotel/hotel.component';
import { HotelsComponent } from './hotel/hotels/hotels.component';
import { RoomsComponent } from './hotel/hotels/rooms/rooms.component';
import { ReservationDetailComponent } from './reservation/reservations-viewer/reservation-detail/reservation-detail.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'entry', children: [
    { path: '', component: EntryComponent, pathMatch: 'full' },
    { path: 'sign-in', component: SignInComponent },
    { path: 'sign-up', component: SignUpComponent }
  ]},
  { path: 'reservations', component: ReservationComponent, canActivate: [AuthGuard], children: [
    { path: 'create', component:  ReservationCreatorComponent, outlet: 'reservations' },
    { path: 'view', component:  ReservationsViewerComponent, outlet: 'reservations' },
    { path: ':reservationId', component:  ReservationDetailComponent, outlet: 'reservations' },
  ]},
  { path: 'hotels', component: HotelComponent, canActivate: [AuthGuard], children: [
    { path: 'view', component: HotelsComponent, outlet: 'hotels' },
    { path: ':hotelId/rooms', component: RoomsComponent, outlet: 'hotels' } ]
  },
  { path: 'users/:userId', component: UserComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})
export class AppRouterModule { }
