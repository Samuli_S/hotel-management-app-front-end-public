import { Component, OnInit, OnDestroy } from '@angular/core';
import { HotelService } from '../../hotel/hotel.service';
import { ReservationService } from '../reservation.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-reservations-viewer',
  templateUrl: './reservations-viewer.component.html',
  styleUrls: ['./reservations-viewer.component.css']
})
export class ReservationsViewerComponent implements OnInit, OnDestroy {
  hotels;
  reservations;
  shouldShowList: boolean;
  selectedHotelId;
  hotelSubscription: Subscription;
  reservationSubscription: Subscription;

  constructor(private hotelService: HotelService,
              private reservationService: ReservationService,
              private router: Router) { }

  ngOnInit() {
    this.shouldShowList = false;
    this.hotelSubscription = this.hotelService.getHotels()
      .subscribe(hotels => {
        this.hotels = hotels;
      });
  }
  onSelectHotel(hotelId) {
    this.selectedHotelId = hotelId;
    this.reservationSubscription = this.reservationService.getHotelReservations(hotelId)
      .subscribe(reservations => {
        this.reservations = reservations;
        this.shouldShowList = true;
      });
  }

  onReservationSelected(reservation) {
    this.router.navigate(['/reservations',
      {
        outlets: {
          reservations:  [reservation._id]
        }
      }],
      {
        queryParams: {
          hotelId: this.selectedHotelId,
          roomId: reservation.room
      }
    });
  }

  ngOnDestroy() {
    if (this.hotelSubscription) {
      this.hotelSubscription.unsubscribe();
    }
    if (this.reservationSubscription) {
      this.reservationSubscription.unsubscribe();
    }
  }
}
