import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ReservationService } from '../../reservation.service';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { mergeMap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/zip';
import 'rxjs/add/operator/switchMap';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-reservation-detail',
  templateUrl: './reservation-detail.component.html',
  styleUrls: ['./reservation-detail.component.css']
})
export class ReservationDetailComponent implements OnInit, OnDestroy {
  reservation;
  reservationSubscription: Subscription;
  isLoading: boolean;

  constructor(private route: ActivatedRoute,
              private reservationService: ReservationService) {
    this.isLoading = true;
  }

  ngOnInit() {

    this.reservationSubscription = Observable.zip(this.route.paramMap, this.route.queryParamMap)
      .switchMap(results => {
        this.isLoading = true;
        const reservationId = results['0'].get('reservationId');
        const hotelId = results['1'].get('hotelId');
        const roomId = results['1'].get('roomId');
        return this.reservationService.getReservation(reservationId, hotelId, roomId);
      })
      .subscribe(reservation => {
        this.reservation = reservation;
        this.isLoading = false;
      });
  }

  ngOnDestroy() {
    this.reservationSubscription.unsubscribe();
  }
}
