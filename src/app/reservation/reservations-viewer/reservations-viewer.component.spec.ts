import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationsViewerComponent } from './reservations-viewer.component';

describe('ReservationsViewerComponent', () => {
  let component: ReservationsViewerComponent;
  let fixture: ComponentFixture<ReservationsViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservationsViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationsViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
