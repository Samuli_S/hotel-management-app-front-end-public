import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-reservations-list',
  templateUrl: './reservations-list.component.html',
  styleUrls: ['./reservations-list.component.css']
})

export class ReservationsListComponent implements OnInit {
  @Input() reservations;
  @Output() reservationSelected: EventEmitter<any>;

  constructor() {
    this.reservationSelected = new EventEmitter<any>();
  }

  ngOnInit() {
  }

}
