import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-customer-details-form',
  templateUrl: './customer-details-form.component.html',
  styleUrls: ['./customer-details-form.component.css']
})
export class CustomerDetailsFormComponent implements OnInit {
  @Input() allowSubmits;
  @Output() creationAction: EventEmitter<any>;

  customerForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.creationAction = new EventEmitter<any>();
  }

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm() {
    this.customerForm = this.formBuilder.group({
      firstName: ['', [ Validators.required ]],
      lastName: ['', [ Validators.required ]],
      phoneNumber: ['', [ Validators.required ]],
      email: ['', [ Validators.required, Validators.email ]],
      address: this.formBuilder.group({
        street: ['', Validators.required],
        postalCode: ['', Validators.required],
        city: ['', Validators.required]
      })
    });
  }

  onCreate() {
    const customer = this.customerForm.value;
    this.creationAction.emit(customer);
  }

  onCancel() {
    this.creationAction.emit(null);
  }
}
