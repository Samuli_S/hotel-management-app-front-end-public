import { Component, OnInit, OnDestroy } from '@angular/core';
import { HotelService } from '../../hotel/hotel.service';
import { RoomService } from '../../hotel/hotels/rooms/room.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ReservationService } from '../reservation.service';
import { toast } from 'angular2-materialize';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-reservation-creator',
  templateUrl: './reservation-creator.component.html',
  styleUrls: ['./reservation-creator.component.css']
})
export class ReservationCreatorComponent implements OnInit, OnDestroy {
  stepIndex; // The step in reservation creation process.
  reservationSteps: string[];
  hotels;
  selectedHotelId;
  rooms;
  selectedRoomId;
  isSendingReservation;
  hotelSubscription: Subscription;
  roomSubscription: Subscription;
  reservationSubscription: Subscription;

  constructor(private hotelService: HotelService,
              private roomService: RoomService,
              private reservationService: ReservationService,
              private formBuilder: FormBuilder,
              private router: Router) {
    this.reservationSteps = [
      'SELECT_HOTEL',
      'SELECT_ROOM',
      'FILL_IN_CUSTOMER_DETAILS'
    ];
    this.stepIndex = 0;
  }

  ngOnInit() {
    this.hotelSubscription = this.hotelService.getHotels()
      .subscribe(hotels => {
        this.hotels = hotels;
      });
  }

  onNextStep() {
    if (this.stepIndex === this.reservationSteps.length - 1) {
      return;
    }
    this.stepIndex++;
  }

  onPreviousStep() {
    if (this.stepIndex === 0) {
      return;
    }
    this.stepIndex--;
  }

  onSelectHotel(hotelId) {
    this.selectedHotelId = hotelId;
    this.onNextStep();
    this.roomSubscription = this.roomService.getNotReservedHotelRooms(hotelId)
      .subscribe(rooms => {
        this.rooms = rooms;
      });
  }

  onRoomSelected(room) {
    this.selectedRoomId = room._id;
    this.onNextStep();
  }

  onCreationAction(customer) {
    if (!customer) {
      // Customer creation was cancelled.
      this.router.navigate(['/home']);
      return;
    }
    this.isSendingReservation = true;
    this.reservationSubscription = this.reservationService
      .createReservation(this.selectedHotelId, this.selectedRoomId, customer)
      .subscribe(
        response => {
          toast(`Created reservation for ${customer['lastName']}`, 4000, 'rounded');
          const reservationId = response['_id'];
          this.router.navigate(['/reservations',
          {
            outlets: {
              reservations:  [reservationId]
            }
          }],
          {
            queryParams: {
              hotelId: this.selectedHotelId,
              roomId: this.selectedRoomId
          }
        });
        },
        err => {
          console.error(err);
          toast(`Reservation creation  error (${err.error.apiError.name}) - please try again later.`, 10000, 'rounded');
          this.isSendingReservation = false;
        }
      );
  }

  ngOnDestroy() {
    if (this.hotelSubscription) {
      this.hotelSubscription.unsubscribe();
    }
    if (this.roomSubscription) {
      this.roomSubscription.unsubscribe();
    }
    if (this.reservationSubscription) {
      this.reservationSubscription.unsubscribe();
    }
  }
}
