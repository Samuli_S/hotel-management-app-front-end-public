import { Injectable } from '@angular/core';
import { AuthService } from '../core/auth/auth.service';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/retry';

@Injectable()
export class ReservationService {
  authHeader;

  constructor(private authService: AuthService,
              private http: HttpClient) {
   this.authHeader = this.authService.getAuthHeader();
  }

  createReservation(hotelId, roomId, reservation) {
    return this.http.post(`/api/v1/hotels/${hotelId}/rooms/${roomId}/reservations`, {
      customer: reservation
    },
    {
      headers: this.authHeader
    })
    .retry(3);
  }

  getReservation(reservationId, hotelId, roomId) {
    return this.http.get(`/api/v1/hotels/${hotelId}/rooms/${roomId}/reservations/${reservationId}`,
    {
      headers: this.authHeader
    })
    .retry(3);
  }

  getHotelReservations(hotelId) {
    return this.http.get(`/api/v1/hotels/${hotelId}/reservations`,
    {
      headers: this.authHeader
    })
    .retry(3);
  }
}
