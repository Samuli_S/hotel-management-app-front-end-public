import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { MaterializeModule } from 'angular2-materialize';

import { CoreModule } from './core/core.module';
import { AppRouterModule } from './app-router.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { EntryComponent } from './entry/entry.component';
import { SignInComponent } from './entry/sign-in/sign-in.component';
import { SignUpComponent } from './entry/sign-up/sign-up.component';
import { ReservationComponent } from './reservation/reservation.component';
import { ReservationCreatorComponent } from './reservation/reservation-creator/reservation-creator.component';
import { ReservationsViewerComponent } from './reservation/reservations-viewer/reservations-viewer.component';
import { HotelComponent } from './hotel/hotel.component';
import { HotelService } from './hotel/hotel.service';
import { HotelsTableComponent } from './hotel/hotels/hotels-table/hotels-table.component';
import { HotelsComponent } from './hotel/hotels/hotels.component';
import { RoomsComponent } from './hotel/hotels/rooms/rooms.component';
import { RoomsTableComponent } from './hotel/hotels/rooms/rooms-table/rooms-table.component';
import { RoomService } from './hotel/hotels/rooms/room.service';
import { CustomerDetailsFormComponent } from './reservation/reservation-creator/customer-details-form/customer-details-form.component';
import { ReservationService } from './reservation/reservation.service';
import { ReservationDetailComponent } from './reservation/reservations-viewer/reservation-detail/reservation-detail.component';
import { ReservationsListComponent } from './reservation/reservations-viewer/reservations-list/reservations-list.component';
import { UserComponent } from './user/user.component';
import { UserService } from './user/user.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    EntryComponent,
    SignInComponent,
    SignUpComponent,
    ReservationComponent,
    ReservationCreatorComponent,
    ReservationsViewerComponent,
    HotelComponent,
    HotelsTableComponent,
    RoomsComponent,
    HotelsComponent,
    RoomsComponent,
    RoomsTableComponent,
    CustomerDetailsFormComponent,
    ReservationDetailComponent,
    ReservationsListComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CoreModule,
    FormsModule,
    AppRouterModule,
    MaterializeModule
  ],
  providers: [HotelService, RoomService, ReservationService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
