import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/do';

@Injectable()
export class AuthService {
  constructor(private http: HttpClient) { }

  getAuthHeader(): HttpHeaders {
    let token = localStorage.getItem('token');
    if (!token) {
      return null;
    }
    let header = new HttpHeaders();
    // Remove quatation marks ("") from the token.
    token = token.replace(/['"]+/g, '');
    header = header.set('Authorization', `Bearer ${token}`);
    return header;
  }

  getSignedInUser(): Object {
    const userString = localStorage.getItem('user');
    if (!userString) {
      return null;
    }
    const user = JSON.parse(userString);
    return user;
  }

  isUserSignedIn() {
    return localStorage.getItem('token') !== null;
  }

  signUp(email: string, password: string): Observable<Object> {
    return this.http.post('api/v1/auth/sign-up', { email, password })
      .retry(3)
      .do(response => {
        if (!response['error']) {
          this.initializeStorage(response);
        }
      });
  }

  signIn(email: string, password: string): Observable<Object> {
    return this.http.post('api/v1/auth/sign-in', { email, password })
      .retry(3)
      .do(response => {
        if (!response['error']) {
          this.initializeStorage(response);
        }
      });
  }

  signOut() {
    this.clearStorage();
  }

  private initializeStorage(data: Object) {
    localStorage.setItem('token', JSON.stringify(data['token']));
    localStorage.setItem('user', JSON.stringify(data['user']));
  }

  private clearStorage() {
    localStorage.clear();
  }
}
