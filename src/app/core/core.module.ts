import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AuthService } from './auth/auth.service';
import { AuthGuard } from './auth/auth.guard';

import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule
  ],
  declarations: [
    NavbarComponent,
    FooterComponent
  ],
  providers: [
    AuthService,
    AuthGuard
  ],
  exports: [
    ReactiveFormsModule,
    NavbarComponent,
    FooterComponent
  ]
})
export class CoreModule { }
