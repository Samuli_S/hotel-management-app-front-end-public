import { Component, OnInit, OnDestroy } from '@angular/core';
import { HotelService } from './hotel.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.css']
})
export class HotelComponent implements OnInit, OnDestroy {
  hotels;
  hotelSubsription: Subscription;

  constructor(private hotelService: HotelService) {
    this.hotels = null;
  }

  ngOnInit() {
    this.hotelSubsription = this.hotelService.getHotels()
      .subscribe(hotels => {
        this.hotels = hotels;
      });
  }

  ngOnDestroy() {
    this.hotelSubsription.unsubscribe();
  }
}
