import { Injectable } from '@angular/core';
import { AuthService } from '../core/auth/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class HotelService {

  constructor(private authService: AuthService,
              private http: HttpClient) { }

  getHotels() {
    const authHeader = this.authService.getAuthHeader();
    return this.http.get('/api/v1/hotels', { headers:  authHeader});
  }

}
