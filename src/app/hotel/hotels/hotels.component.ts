import { Component, OnInit, OnDestroy } from '@angular/core';
import { HotelService } from '../hotel.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent implements OnInit, OnDestroy {
  hotels;
  hotelSubscription: Subscription;

  constructor(private hotelService: HotelService,
              private route: ActivatedRoute,
              private router: Router) {
    this.hotels = null;
  }

  ngOnInit() {
    this.hotelSubscription = this.hotelService.getHotels()
      .subscribe(hotels => {
        this.hotels = hotels;
      });
  }

  onHotelSelected(hotel) {
    const hotelId = hotel._id;
    const hotelName = hotel.name;
    this.router.navigate(['/hotels', { outlets: { hotels:  [hotelId, 'rooms']} } ], { queryParams: { hotelName} });
  }

  ngOnDestroy() {
    this.hotelSubscription.unsubscribe();
  }
}
