import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hotels-table',
  templateUrl: './hotels-table.component.html',
  styleUrls: ['./hotels-table.component.css']
})
export class HotelsTableComponent implements OnInit {
  @Input() hotels;
  @Output() hotelSelected: EventEmitter<any>;

  constructor() {
    this.hotelSelected = new EventEmitter<any>();
  }

  ngOnInit() {
  }
}
