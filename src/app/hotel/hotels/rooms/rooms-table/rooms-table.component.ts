import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-rooms-table',
  templateUrl: './rooms-table.component.html',
  styleUrls: ['./rooms-table.component.css']
})
export class RoomsTableComponent implements OnInit {
  @Input() rooms;
  @Output() roomSelected: EventEmitter<any>;

  constructor() {
    this.roomSelected = new EventEmitter<any>();
  }

  ngOnInit() {
  }
}
