import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RoomService } from './room.service';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit {
  rooms;
  hotelName: string;

  constructor(private route: ActivatedRoute,
              private roomService: RoomService) { }

  ngOnInit() {
    this.route.paramMap
      .subscribe(params => {
        const hotelId = params.get('hotelId');
        this.roomService.getHotelRooms(hotelId)
        .subscribe(rooms => {
          this.rooms = rooms;
        });
      });
    this.route.queryParamMap
      .subscribe(params => {
        this.hotelName = params.get('hotelName');
      });
  }
}
