import { Injectable } from '@angular/core';
import { AuthService } from '../../../core/auth/auth.service';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class RoomService {
  authHeader;

  constructor(private authService: AuthService,
              private http: HttpClient) {
    this.authHeader = this.authService.getAuthHeader();
  }

  getNotReservedHotelRooms(hotelId: string) {
    const params: HttpParams = new HttpParams().set('type', 'notReserved');
    return this.http.get(`api/v1/hotels/${hotelId}/rooms`, {
      headers: this.authHeader,
      params
    });
  }

  getHotelRooms(hotelId: string) {
    return this.http.get(`api/v1/hotels/${hotelId}/rooms`, {
      headers: this.authHeader
    });
  }
}
