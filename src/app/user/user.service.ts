import { Injectable } from '@angular/core';
import { AuthService } from '../core/auth/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserService {
  authHeader;

  constructor(private authService: AuthService,
              private http: HttpClient) {
    this.authHeader = this.authService.getAuthHeader();
  }

  getUser(id: string) {
    return this.http.get(`/api/v1/users/${id}`, { headers: this.authHeader });
  }
}
