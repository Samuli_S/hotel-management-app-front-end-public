import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../core/auth/auth.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: [
    './sign-in.component.css',
    '../entry.component.css'
  ]
})
export class SignInComponent implements OnInit, OnDestroy {
  signInForm: FormGroup;
  isLoading: boolean;
  error: Object;
  authSubscription: Subscription;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.createsignInForm();
  }

  createsignInForm() {
    this.signInForm = this.formBuilder.group({
      email: ['', [ Validators.email, Validators.required ]],
      password: ['', [ Validators.required ]],
    });
  }

  onSignIn() {
    this.signInForm.disable();
    this.isLoading = true;
    const email = this.signInForm.value.email;
    const password = this.signInForm.value.password;
    this.authSubscription = this.authService.signIn(email, password)
      .subscribe(
        response => {
          const user = this.authService.getSignedInUser();
          this.router.navigate(['/home']);
        },
        err => {
          console.log('sign in error', err);
          this.error = err.error.apiError;
          this.isLoading = false;
          this.signInForm.get('password').reset();
          this.signInForm.enable();
        }
      );

  }

  ngOnDestroy() {
    if (!this.authSubscription) {
      return;
    }
    this.authSubscription.unsubscribe();
  }
}
