import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AuthService } from '../../core/auth/auth.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: [
    './sign-up.component.css',
    '../entry.component.css'
  ]
})
export class SignUpComponent implements OnInit, OnDestroy {
  signUpForm: FormGroup;
  isLoading: boolean;
  error: Object;
  authSubscription: Subscription;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) {
    this.isLoading = false;
    this.error = null;
  }

  ngOnInit() {
    this.createSignUpForm();
  }

  createSignUpForm() {
    this.signUpForm = this.formBuilder.group({
      email: ['', [ Validators.email, Validators.required ]],
      password: ['', [ Validators.required, Validators.minLength(6) ]],
    });
  }

  onSignUp() {
    this.signUpForm.disable();
    this.isLoading = true;
    const email = this.signUpForm.value.email;
    const password = this.signUpForm.value.password;
    this.authSubscription = this.authService.signUp(email, password)
      .subscribe(
        response => {
          const user = this.authService.getSignedInUser();
          this.router.navigate(['/home']);
        },
        err => {
          console.log('sign up error', err);
          this.error = err.error.apiError;
          this.isLoading = false;
          this.signUpForm.get('password').reset();
          this.signUpForm.enable();
        }
      );
  }

  ngOnDestroy() {
    if (!this.authSubscription) {
      return;
    }
    this.authSubscription.unsubscribe();
  }
}
