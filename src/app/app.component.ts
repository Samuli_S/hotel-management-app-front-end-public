import { Component, OnInit } from '@angular/core';
import { AuthService } from './core/auth/auth.service';
import { Subscription } from 'rxjs/Subscription';
import { HttpResponse } from '@angular/common/http';
import { Router, NavigationStart, Event, NavigationEnd, NavigationError } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  isLoading: boolean;

  constructor(public authService: AuthService,
              private router: Router) {
    this.isLoading = false;
  }

  ngOnInit() {
    // This is implemented according to reacting to router events example:
    // https://medium.com/@Carmichaelize/detecting-router-changes-with-angular-2-2f8c019788c3
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        this.isLoading = true;
      } else {
        this.isLoading = false;
      }
    });
  }
}
