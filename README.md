# README #

Hotel Management App (HMA) prototype front end.

### What does this repository contain? ###

* This repository contains Hotel Management App Angular 5 front end.
* The app allows users of some imaginary hotel industry related company to manage hotel room reservations accross hotels.
* The users can create and view reservations in addition to viewing room and user details.
* Angular2 Materialize is used to implement a higher quality UX with clear interface elements and workflows.
* Hotel Management App back end's RESTful API is consumed by this app and it is also served from this back end.
* json web tokens (jwt) as used as an authentication mechanism for accessing the API after sign up or sign in.
* Angular CLI was used to generate the base structure and build processes of this app.

### How do I get set up? ###

* Clone this repository.
* Run at app root > NPM install
* Ensure that you have Angular CLI installed
* Run at app root > ng serve
* Or run  "npm start" in order to use the proxy configuration that routes the app to localhost:3000 (HMA back end expected)

### Ownership ###

* Repository owner: Samuli Siitonen